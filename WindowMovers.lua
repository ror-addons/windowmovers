WindowMovers = {}
WindowMovers.Settings = {}

function WindowMovers.OnInitialize()
  
   WindowMovers.HookMovement()
   WindowMovers.HookCharacter()
   WindowMovers.HookBackpack()
   WindowMovers.HookTalisman()
   WindowMovers.HookApothecary()
   WindowMovers.HookCultivation()
   
end

--[[ *********************
     *  Cascading Hooks  *
     ********************* --]] 
     
function WindowMovers.HookMovement()
   WindowMovers.OnShownDefault = WindowUtils.OnShown 
   WindowUtils.OnShown = WindowMovers.OnShownHook
   WindowMovers.AddToOpenListDefault = WindowUtils.AddToOpenList 
   WindowUtils.AddToOpenList = WindowMovers.AddToOpenListHook
end
function WindowMovers.OnShownHook(...)
   local closeCallback = (select(1, ...))
   if closeCallback == CharacterWindow.Hide then return end
   if closeCallback == EA_Window_Backpack.Hide then return end
   return WindowMovers.OnShownDefault(...)
end 
function WindowMovers.AddToOpenListHook(...)
   local windowName = (select(1, ...))
   local currentWindow = CraftingSystem.currentWindow
   if windowName == currentWindow then return end
   WindowMovers.AddToOpenListDefault(...)
end 

--[[ **************************
     *  Open and Close Hooks  *
     **************************  --]] 

-- ****Character Window****
function WindowMovers.HookCharacter()
   --Character Window
   WindowMovers.CharacterHiddenDefault = CharacterWindow.OnHidden 
   CharacterWindow.OnHidden = WindowMovers.CharacterHidden
   WindowMovers.CharacterShownDefault = CharacterWindow.OnShown 
   CharacterWindow.OnShown = WindowMovers.CharacterShown
end
function WindowMovers.CharacterHidden(...)
   WindowMovers.CharacterHiddenDefault(...)
   WindowMovers.WindowMover("CharacterWindow")
end
function WindowMovers.CharacterShown(...)
   WindowMovers.CharacterShownDefault(...)
   WindowMovers.WindowMover("CharacterWindow")
end

-- ****Backpack Window****
function WindowMovers.HookBackpack()
   WindowMovers.BackpackHiddenDefault = EA_Window_Backpack.OnHidden 
   EA_Window_Backpack.OnHidden = WindowMovers.BackpackHidden
   WindowMovers.BackpackShownDefault = EA_Window_Backpack.OnShown 
   EA_Window_Backpack.OnShown = WindowMovers.BackpackShown
end
function WindowMovers.BackpackHidden(...)
   WindowMovers.BackpackHiddenDefault(...)
   WindowMovers.WindowMover("EA_Window_Backpack")
end
function WindowMovers.BackpackShown(...)
   WindowMovers.BackpackShownDefault(...)
   WindowMovers.WindowMover("EA_Window_Backpack")
end

-- ****Talisman Window****
function WindowMovers.HookTalisman()
   WindowMovers.TalismanHiddenDefault = TalismanMakingWindow.Hide 
   TalismanMakingWindow.Hide = WindowMovers.TalismanHidden
   WindowMovers.TalismanShownDefault = TalismanMakingWindow.Show 
   TalismanMakingWindow.Show = WindowMovers.TalismanShown
end
function WindowMovers.TalismanShown(...)
   WindowMovers.TalismanShownDefault(...)
   WindowMovers.WindowMover("TalismanMakingWindow")
end
function WindowMovers.TalismanHidden(...)
   WindowMovers.TalismanHiddenDefault(...)
   WindowMovers.WindowMover("TalismanMakingWindow")
end

-- ****Apothecary Window****
function WindowMovers.HookApothecary()
   WindowMovers.ApothecaryHiddenDefault = ApothecaryWindow.Hide 
   ApothecaryWindow.Hide = WindowMovers.ApothecaryHidden
   WindowMovers.ApothecaryShownDefault = ApothecaryWindow.Show 
   ApothecaryWindow.Show = WindowMovers.ApothecaryShown
end
function WindowMovers.ApothecaryShown(...)
   WindowMovers.ApothecaryShownDefault(...)
   WindowMovers.WindowMover("ApothecaryWindow")
end
function WindowMovers.ApothecaryHidden(...)
   WindowMovers.ApothecaryHiddenDefault(...)
   WindowMovers.WindowMover("ApothecaryWindow")
end

-- ****Cultivation Window****
function WindowMovers.HookCultivation()
   WindowMovers.CultivationHiddenDefault = CultivationWindow.Hide 
   CultivationWindow.Hide = WindowMovers.CultivationHidden
   WindowMovers.CultivationShownDefault = CultivationWindow.Show 
   CultivationWindow.Show = WindowMovers.CultivationShown
end
function WindowMovers.CultivationShown(...)
   WindowMovers.CultivationShownDefault(...)
   WindowMovers.WindowMover("CultivationWindow")
end
function WindowMovers.CultivationHidden(...)
   WindowMovers.CultivationHiddenDefault(...)
   WindowMovers.WindowMover("CultivationWindow")
end

--[[ **************
     *  The Core  *
     ************** --]] 

function WindowMovers.WindowMover(window)
   if WindowGetShowing(window) then
      WindowMovers.LoadPosition(window)
   end
   WindowMovers.SavePosition(window)
end

function WindowMovers.LoadPosition(window)
    if WindowMovers.Settings[window] == nil then
       WindowMovers.SavePosition(window)
    end
    WindowClearAnchors(window)
    WindowAddAnchor(window,
       WindowMovers.Settings[window]["point"],
       WindowMovers.Settings[window]["relwin"],
       WindowMovers.Settings[window]["relpoint"],
       WindowMovers.Settings[window]["x"],
       WindowMovers.Settings[window]["y"])
    WindowSetScale(window, WindowMovers.Settings[window]["scale"])
    WindowSetMovable(window, true)
end

function WindowMovers.SavePosition(window)
    if WindowMovers.Settings[window] == nil then
       WindowMovers.Settings[window] = {}
    end
    local point, relpoint, relwin, x, y = WindowGetAnchor(window, 1)
    WindowMovers.Settings[window]["point"] = point
    WindowMovers.Settings[window]["relpoint"] = relpoint
    WindowMovers.Settings[window]["relwin"] = relwin
    WindowMovers.Settings[window]["x"] = x
    WindowMovers.Settings[window]["y"] = y
    WindowMovers.Settings[window]["scale"] = WindowGetScale(window)
end

