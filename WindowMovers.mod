<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="WindowMovers" version="1.2" date="10/10/2008">
        <Author name="Felyza" email="felyza@gmail.com" />
        <Description text="Allows moving of various screen elements." />
 	<Dependencies>
		<Dependency name="EA_CharacterWindow"/>
		<Dependency name="EA_CultivationWindow"/>
		<Dependency name="EA_BackpackWindow"/>
		<Dependency name="EA_CraftingSystem"/>
 	</Dependencies>
       <Files>
            <File name="WindowMovers.lua" />
        </Files>
        <OnInitialize>
             <CallFunction name="WindowMovers.OnInitialize" />
        </OnInitialize>
        <OnUpdate />
        <OnShutdown />
	<SavedVariables>
		<SavedVariable name="WindowMovers.Settings" /> 
	</SavedVariables>
    </UiMod>
</ModuleFile>